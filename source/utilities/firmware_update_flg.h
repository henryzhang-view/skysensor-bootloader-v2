/*
 * firmware_update_flg.h
 *
 *  Created on: 13-Oct-2017
 *      Author: Srinivas.A
 */

#ifndef FIRMWARE_UPDATE_FLG_H_
#define FIRMWARE_UPDATE_FLG_H_

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define APP_PARAMS_FW_UPDATE_FLG_SIZE  2

/* On-chip Flash memory start address. */
#ifndef APP_CFG_CPU_FLASH_ADDRESS
     #define APP_CFG_CPU_FLASH_ADDRESS                  (0x0U)
#endif

/* On-chip Flash size.*/
#define APP_CFG_CPU_FLASH_SIZE                          (1024U * 1024U)    /* 1 MB */

/* Smallest logical block which can be erased independently.*/
#define APP_CFG_CPU_FLASH_PAGE_SIZE            		    (4U*1024U)        /* 4KB sector.*/

/* Block size reserved for application parameters stored in flash.*/
#ifndef APP_CFG_FLASH_PARAMS_SIZE
    #define APP_CFG_FLASH_PARAMS_SIZE      				APP_CFG_CPU_FLASH_PAGE_SIZE
#endif


/* Pointer to application parameters stored in flash.*/
#ifndef APP_CFG_FLASH_PARAMS_ADDRESS
    #define APP_CFG_FLASH_PARAMS_ADDRESS   (APP_CFG_CPU_FLASH_ADDRESS + APP_CFG_CPU_FLASH_SIZE - APP_CFG_FLASH_PARAMS_SIZE) /* Last sector of the flash.*/
#endif

/**************************************************************************/ /*!
 * @brief Parameters-version string.@n
 * It defines version of the parameter structure saved in a persistent storage.
 ******************************************************************************/
#define APP_PARAMS_VERSION                 "01" /* Changed on any change in the param. structures.*/

/**************************************************************************/ /*!
 * @brief Signature string value.@n
 * It's used for simple check if configuration structure is present
 * in a persistant storage.
 ******************************************************************************/
#define APP_PARAMS_SIGNATURE               "SH"APP_PARAMS_VERSION

/**************************************************************************/ /*!
 * @brief The maximum length of the signature.
 ******************************************************************************/
#define APP_PARAMS_SIGNATURE_SIZE          (16U)

/**************************************************************************/ /*!
 * @brief Boot mode.
 ******************************************************************************/
typedef enum
{
    APP_PARAMS_FWUPDATE_MODE_DISABLED      = (0),  /**< @No Need to Update Application Firmware. */
    APP_PARAMS_FWUPDATE_MODE_ENABLED       = (1),  /**< @Application Firmware has to be Updated */
}
fwupdate_params_mode_t;

/*******************************************************************************
 * Prototypes
 ******************************************************************************/


/*******************************************************************************
 * Variables
 ******************************************************************************/
/**************************************************************************/ /*!
 * @brief Application parameter structure used to save the bootloader
 * specific configuration to a persistent storage.
 ******************************************************************************/
struct app_params_fwupdate
{
	uint32_t update_flg[APP_PARAMS_FW_UPDATE_FLG_SIZE];     /**< @brief Upgrade.@n
	                                                      	  * It's used to indicate the bootloader that
	                                                      	  * a new application firmware image is present
	                                                      	  * which has to be downloaded on next power up/reseet.*/

	fwupdate_params_mode_t mode;
};

struct crc_header
{
    uint32_t tag; //!< [00:03] Tag value used to validate the bootloader configuration data. Must be set to 'kcfg'.
    uint32_t crcStartAddress;  //!< [04:07]
    uint32_t crcByteCount;     //!< [08:0b]
    uint32_t crcExpectedValue; //!< [0c:0f]
};

/**************************************************************************/ /*!
 * @brief Main application  parameter structure used to save the
 * application specific configuration to a persistent storage.
 ******************************************************************************/
struct app_params_flash
{
	char signature[APP_PARAMS_SIGNATURE_SIZE];         /**< @brief Signature string.@n
		                                                 * It's used for simple check if configuration
		                                                 * structure is present in a persistent storage.*/
    struct app_params_fwupdate fwupdate_params;        /**< @brief FNET TCP/IP stack specific
                                                 	 	 * configuration parameters.*/
    struct crc_header crc_header_t;
};


#endif /* FIRMWARE_UPDATE_FLG_H_ */
